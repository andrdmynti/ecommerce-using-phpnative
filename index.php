<?php

	include 'action/connection.php';

	session_start();

	if(isset($_GET['content'])) $content = $_GET['content']; 
	    else $content = "index";	


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Holomoc </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">
	<link href="css/v.1.3.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous"> -->
	<script>
		function magnify(imgID, zoom) {
			  var img, glass, w, h, bw;
			  img = document.getElementById(imgID);
			  /*create magnifier glass:*/
			  glass = document.createElement("DIV");
			  glass.setAttribute("class", "img-magnifier-glass");
			  /*insert magnifier glass:*/
			  img.parentElement.insertBefore(glass, img);
			  /*set background properties for the magnifier glass:*/
			  glass.style.backgroundImage = "url('" + img.src + "')";
			  glass.style.backgroundRepeat = "no-repeat";
			  glass.style.backgroundSize = (img.width * zoom) + "px " + (img.height * zoom) + "px";
			  bw = 3;
			  w = glass.offsetWidth / 2;
			  h = glass.offsetHeight / 2;
			  /*execute a function when someone moves the magnifier glass over the image:*/
			  glass.addEventListener("mousemove", moveMagnifier);
			  img.addEventListener("mousemove", moveMagnifier);
			  /*and also for touch screens:*/
			  glass.addEventListener("touchmove", moveMagnifier);
			  img.addEventListener("touchmove", moveMagnifier);
			  function moveMagnifier(e) {
			    var pos, x, y;
			    /*prevent any other actions that may occur when moving over the image*/
			    e.preventDefault();
			    /*get the cursor's x and y positions:*/
			    pos = getCursorPos(e);
			    x = pos.x;
			    y = pos.y;
			    /*prevent the magnifier glass from being positioned outside the image:*/
			    if (x > img.width - (w / zoom)) {x = img.width - (w / zoom);}
			    if (x < w / zoom) {x = w / zoom;}
			    if (y > img.height - (h / zoom)) {y = img.height - (h / zoom);}
			    if (y < h / zoom) {y = h / zoom;}
			    /*set the position of the magnifier glass:*/
			    glass.style.left = (x - w) + "px";
			    glass.style.top = (y - h) + "px";
			    /*display what the magnifier glass "sees":*/
			    glass.style.backgroundPosition = "-" + ((x * zoom) - w + bw) + "px -" + ((y * zoom) - h + bw) + "px";
			  }
			  function getCursorPos(e) {
			    var a, x = 0, y = 0;
			    e = e || window.event;
			    /*get the x and y positions of the image:*/
			    a = img.getBoundingClientRect();
			    /*calculate the cursor's x and y coordinates, relative to the image:*/
			    x = e.pageX - a.left;
			    y = e.pageY - a.top;
			    /*consider any page scrolling:*/
			    x = x - window.pageXOffset;
			    y = y - window.pageYOffset;
			    return {x : x, y : y};
			  }
			}
		</script>
</head><!--/head-->

<body>
	<div class="site-wrapper-alternative">
		<div class="container-fluid" id="home">
			<nav class="navbar navbar-expand-lg navbar-dark">
				<ul class="navbar-brand ml-4 pull-left">
					<li><a href="index.php"><img src="images/home/logo.png"></a></li>
				</ul>
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<p></p>
					</li>
				</ul>
			<!-- </div> -->
				<!-- <div class="navbar-header"> -->
				<button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".navbar-collapse">
					<i class="fa fa-bars"  style="color:#fff; font-size:20px;"></i></span>
				</button>
				<!-- </div> -->
				<div class="navbar-collapse shop-menu" id="navbarNav">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<p></p>
						</li>
					</ul>
					<ul class="navbar-nav pull-right">
						<li class="nav-item active">
							<div class="search_box pull-left">
								<input type="text" placeholder="Search"/>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link pull-right" id="features" href="index.php?content=shop">Shop <i class="fa fa-crosshairs"></i> </a>
						</li>
						<li class="nav-item">
							<a class="nav-link pull-right" id="features" href="index.php?content=services"> Services <i class="fa fa-pencil-square-o"></i></a>
						</li>
						<li class="nav-item">
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">My Account <i class="fa fa-user"></i></button>
								<ul class="dropdown-menu">
									<li><a href="login.php" style="background-color: rgb(41, 41, 41);"> <p>Wishlist</p></a></li>
									<li><p></p></li>
									<li><a href="login.php" style="background-color: rgb(41, 41, 41);"> <p style="color:rgb(92, 92, 92)">Order & Status</p></a></li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
				<!-- <ul class="navbar-nav pull-right">
					<li class="nav-item">
						<a class="nav-link pull-right" id="features" href="cart.php"><span class="glyphicon glyphicon-shopping-cart"> </span></a>
					</li>
				</ul> -->
			</nav>
		</div>
	</div>
	
	//SLIDER
	<?php

		if ($content=='index')
		  include 'slider.php';
		elseif ($content=='shop')
		  include 'jumbotron_shop.php';
		elseif ($content=='services')
		  include 'jumbotron_services.php';


	?>
	<br><br>
	<section>
		<?php

			if ($content=='index')
				include 'beranda.php';
			elseif ($content=='shop')
		  		include 'shop.php';
			elseif ($content=='services')
		  		include 'services.php';
		  	elseif ($content=='product_details')
		  		include 'product_details.php';
		  	elseif ($content=='tes')
		  		include 'tesdetail.php';

		?>
	</section>
	
	<footer id="footer">
		<!--Footer-->				
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2018 HOLOMOC. All rights reserved.</p>
					<p class="pull-right">Need Help? <span><a target="_blank" href="contact-us.php">Contact Us</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
		<!-- Modal content -->
		
	<center><div id="myModal" class="modal">
		<button type="button" class="close" data-dissmiss="modal"ypu>&times;</button>
		<div class="modal-video modal-lg">
			<video width="800" controls>
				<source src="mov_bbb.mp4" type="video/mp4">
				<source src="mov_bbb.ogg" type="video/ogg">
				Your browser does not support HTML5 video.
			</video>
		</div>
	</div></center>
	

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/main.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<!-- <script>
		$(document).ready(function() {
			$('.menu-icon').click(function(){
			$('.nav-bar').toggleClass('visible');
			});
		}
	</html>script> -->
</body>
</html