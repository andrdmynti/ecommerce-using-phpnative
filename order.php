<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Order & Status | E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<link href="css/v.1.3.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<div class="site-wrapper-alternative">
		<div class="container-fluid" id="home">
			<nav class="navbar navbar-expand-lg navbar-dark">
				<ul class="navbar-brand ml-4">
					<li><a href="index.html"><img src="images/home/logo.png"></a></li>
				</ul>
			<!-- </div> -->
				<!-- <div class="navbar-header"> -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!-- </div> -->
				<div class="collapse navbar-collapse shop-menu" id="navbarNav">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<p></p>
						</li>
					</ul>
					<ul class="navbar-nav ">
						<li class="nav-item">
							<div class="searchbox input-group custom-search-form">
								<input type="text" class="form-control" style="">
								<span class="input-group-btn">
								<button class="btn btn-primary" type="button">
								<span class="glyphicon glyphicon-search"></span>
								</button>
								</span>
							</div>
						</li>
						<li class="nav-item" style="padding-left:150px; margin-top:12px;">
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-user"></i>My Account</button>
								<ul class="dropdown-menu">
									<li><a href="login.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Wishlist</p></a></li>
									<li><p></p></li>
									<li><a href="login.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Order & Status</p></a></li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs"  style="margin-top:90px;">
				<ol class="breadcrumb">
				  <li><a href="#">My Account</a></li>
				  <li class="active">Order History</li>
				</ol>
			</div>
			<div>
                <center><h1>Order History & Status</h1></center>
                <hr>
			</div>
		</div>		
	<div class="container">
		<div class="row">
            <div class="col-sm-4">
                <h3>Your Previous Order</h3>
            </div>
		</div>
		<hr>
		<table class="table">
			<tr style="background-color:rgb(185, 185, 185)">
				<th>
					<center><h5>Order#</h5></center>
				</th>
				<th>
					<center><h5>Purchase Date</h5></center>
				</th>
				<th>
					<center><h5>Amount</h5></center>
				</th>
				<th>
					<center><h5>Destination</h5></center>
				</th>
				<th>
					<center><h5>Status</h5></center>
				</th>
			</tr>
			<tr>
				<td>
					<center><a href="tracking.html"><h5>ID:JS110830138</h5> </a></center>
				</td>
				<td>
					<center><h5>22-02-2022</h5></center>
				</td>
				<td>
					<center><h5>2</h5></center>
				</td>
				<td>
					<center><h5>Jl.kaki naik delman, duduk dimuka 2121<h5></center>
				</td>
				<td>
					<center><h5>Sedang Dikirim</h5></center>
				</td>
			</tr>
		</table>
        <!-- <div class="row bg">
            <div class="col-sm-2">
                <center>Order#</center>
            </div>
            <div class="col-sm-3">
                <center>Purchase Date</center>
            </div>
            <div class="col-sm-2">
                <center>Amount</center>
            </div>
            <div class="col-sm-3">
                <center>Destination</center>
            </div>
            <div class="col-sm-2">
                <center>Status</center>
			</div>
		</div> -->
		<!-- <hr> -->
		<!-- <div class="row">
			<div class="col-sm-2">
				<center><a href="tracking.html">ID:JS110830138 </a></center>
			</div>
			<div class="col-sm-3">
				<center>22-02-2022</center>
			</div>
			<div class="col-sm-2">
				<center>2</center>
			</div>
			<div class="col-sm-3">
				<center>Jl.kaki naik delman, duduk dimuka 2121</center>
			</div>
			<div class="col-sm-2">
				<center>Sedang Dikirim</center>
			</div>
		</div> -->
		<ul class="pagination" style="margin-top:20px;">
			<li><a href="">First</a></li>
			<li><a href="">Previous</a></li>
			<li class="active"><a href="">1</a></li>
			<li><a href="">Next</a></li>
			<li><a href="">Last</a></li>
		</ul>
		<h5 style="font-size:10px;">to check the status of an order not listed above, please enter your order ID and billing email address. This information can be found on the order receipt we emailed you shortly after the order was placed.If your still have question please contact customer service (021)2285 7281 or email info@holomoc.com</h5>
		<div class="row">
			<div class="col-sm-4">
				<p></p>
			</div>
			<div class="box-order col-sm-3">
				<h4>Find An Order</h4>
				<p>order ID required</p>
				<hr>
				<p>Billing Email Address required</p>
				<hr>
				<center><div><input type="submit" value="Find My Order" class="btn btn-primary btn-block btn-lg" tabindex="7"></div></center>
			</div>
	</div>
	</section> <!--/#cart_items-->

	<!-- Modal -->
	<div class="modal fade" id="t_and_c_m" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
				</div>
				<div class="modal-body">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
				</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">I Agree</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</div>

	<footer id="footer"><!--Footer-->

		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2018 HOLOMOC. All rights reserved.</p>
					<p class="pull-right">Need Help?<span><a target="_blank" href="contact-us.html">Contact Us</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->



    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>
