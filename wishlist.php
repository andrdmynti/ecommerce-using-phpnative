<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Wishlist| E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<link href="css/v.1.3.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<div class="site-wrapper-alternative">
		<div class="container-fluid" id="home">
			<nav class="navbar navbar-expand-lg navbar-dark">
				<ul class="navbar-brand ml-4">
					<li><a href="index.html"><img src="images/home/logo.png"></a></li>
				</ul>
			<!-- </div> -->
				<!-- <div class="navbar-header"> -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!-- </div> -->
				<div class="collapse navbar-collapse shop-menu" id="navbarNav">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<p></p>
						</li>
					</ul>
					<ul class="navbar-nav ">
						<li class="nav-item">
							<div class="searchbox input-group custom-search-form">
								<input type="text" class="form-control" style="">
								<span class="input-group-btn">
								<button class="btn btn-primary" type="button">
								<span class="glyphicon glyphicon-search"></span>
								</button>
								</span>
							</div>
						</li>
						<li class="nav-item" style="padding-left:150px; margin-top:12px;">
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-user"></i>My Account</button>
								<ul class="dropdown-menu">
									<li><a href="login.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Wishlist</p></a></li>
									<li><p></p></li>
									<li><a href="login.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Order & Status</p></a></li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs" style="margin-top:85px;">
				<ol class="breadcrumb">
				  <li><a href="#">My Account</a></li>
				  <li class="active">Wishlist</li>
				</ol>
			</div>
			<div>
                <center><h1>Wishlist</h1></center>
                <hr>
			</div>
		</div>		
		<table class="table">
			<tr style="background-color:rgb(185, 185, 185)">
				<th>
					<center><h5>Name Product</h5></center>
				</th>
				<th>
					<center><h5>Description</h5></center>
				</th>
				<th>
					<center><h5>Quantity</h5></center>
				</th>
				<th>
					<center><h5>Destination</h5></center>
				</th>
				<th>
					<center><h5>Prices</h5></center>
				</th>
			</tr>
		</table>
		<!-- <div class="container">
			<div class="row">
				<div class="col-sm-4">
					<h4>No Wishlist Yet</h4>
				</div>
			</div>
			<hr>
			<div class="row bg">
				<div class="col-sm-3">
					<center>Name Product</center>
				</div>
				<div class="col-sm-3">
					<center>Description</center>
				</div>
				<div class="col-sm-3">
					<center>Quantity</center>
				</div>
				<div class="col-sm-3">
					<center>Price</center>
				</div>
			</div>
			<hr>
		</div> -->
	</section> <!--/#cart_items-->

	<!-- Modal -->
	<div class="modal fade" id="t_and_c_m" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
				</div>
				<div class="modal-body">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
				</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">I Agree</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</div>

	<footer id="footer"><!--Footer-->
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2018 HOLOMOC. All rights reserved.</p>
					<p class="pull-right">Need Help?<span><a target="_blank" href="contact-us.html">Contact Us</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->



    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>
