<?php
	
	include 'action/connection.php';

	$id_product = $_GET['id'];

	$edit    = "SELECT * FROM product WHERE id = '$id_product'";
	$hasil   = mysqli_query($connect, $edit)or die(mysqli_error($connect));
	$data    = mysqli_fetch_array($hasil);

?>
<br>
<br>
<br>
<br>

	<!-- <div class="container"> -->
		<div class="breadcrumbs">
			<ol class="breadcrumb">
			<li><a href="#">Homepage</a></li>
			<li><a href="index.php?content=shop">Shop</a></li>
			<li class="active">Product Detail</li>
			</ol>
		</div>
		
			
			<table class="table">								
				<tr class="product-details"><!--product-details-->
					<td class="view-product">
						<div>
							<img src="admin/<?php echo $data['product_image']?>" style="height:100%; padding-bottom:10px;" />
						</div>
					</td>
					<!--/product-information-->
					<td class="product-information">
						<img src="images/product-details/new.jpg" class="newarrival" alt="" />
						<h1 class="product-name"><?php echo $data['product_name']; ?></h1>
						<p>Category: <?php echo $data['id_category_product']; ?></p>
						<img src="images/product-details/rating.png" alt="" />
						<span>90 reviews</span>
						<hr>
						<h1 style="font-size:25px;"><b>IDR <?php echo $data['product_price'] ?></b></h1>
						<br>
						<label style="font-size:15px;">Description:</label>
						<br>
						<table style="width:500px; height:150px;">
							<p style="font-size:15px;">
								<?php

									$query   = "SELECT * FROM product_detail WHERE id_product = '$id_product'";
									$isi     = mysqli_query($connect, $query)or die(mysqli_error($connect));
									$desc  	 = mysqli_fetch_array($isi);

									echo $desc['product_description'];

								?>
							</p>
						</table>
						<br>
						<label style="font-size:15px;">Quantity:</label><br>
						<a class="cart_quantity_down" href="" style="font-size:25px;"> - </a>
						<input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
						<a class="cart_quantity_up" href="" style="font-size:25px;"> + </a>
						<br>
						<br>
						<br>
						<button type="button" class="btn btn-fefault cart">
						<div>
												Add to cart
						</div>
						</button>
						<button type="button" id="myBtn" class="btn btn-fefault cart">
						<div>
												Buy Now
						</div>
						</button>
						<br>
						<p><i class="fa fa-check"></i><b>Available Product</b></p>
						<br>
					</td>
				</tr>
			</table>
			
			<div class="col-xs-12 col-md-5" style="margin-bottom:40px;">
				<div id="similar-product" class="carousel slide" data-ride="carousel">
				<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<div class="item active">
							<?php

								$query   = "SELECT * FROM product_detail WHERE id_product = '$id_product'";
								$isi     = mysqli_query($connect, $query)or die(mysqli_error($connect));
								$tampil  = mysqli_fetch_array($isi);


							?>
							<center>
								<a href="">
									<img src="admin/<?php echo $tampil['product_image_1'] ?>" alt="" style="width:80px;">
								</a>
								<a href="">
									<img src="admin/<?php echo $tampil['product_image_2'] ?>" alt="" style="width:80px;">
								</a>
								<a href="">
									<img src="admin/<?php echo $tampil['product_image_3'] ?>" alt="" style="width:80px;">
								</a>
								<a href="">
									<img src="admin/<?php echo $tampil['product_image_4'] ?>" alt="" style="width:80px;">
								</a>
							</center>
						</div>					
					</div>

					<!-- Controls -->
					<a class="left item-control" href="#similar-product" data-slide="prev">
						<i class="fa fa-angle-left"></i>
					</a>
					<a class="right item-control" href="#similar-product" data-slide="next">
						<i class="fa fa-angle-right"></i>
					</a>
				</div>
			</div>
		<div class="container">
			<div class="category-tab shop-details-tab">
				<ul class="nav nav-tabs">
					<li  class="active"><a href="#details" data-toggle="tab">Detail Product</a></li>
					<li><a href="#feature" data-toggle="tab">Features</a></li>
					<li><a href="#reviews" data-toggle="tab">Reviews (0)</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade in" id="details">
						<table class="table">
							<tr>
								<td>
									<h4>INFORMATION</h4>
								</td>
								<td>	
									<p>Condition :</p>
									<p>Stock     :</p>
									<p>Seen     :</p>
								</td>
								<td>
									<p>Favorite</p>
									<p>Update :</p>
									<p>Status :</p>
								</td>
							</tr>
	
							<tr>
								<td class="cart_product">
									<h4>DESCRIPTION</h4>
								</td>
								<td class="cart_description">
									<p>Lorem ipsum dolor sit amet, has putant ceteros mnesarchum eu.</p>
									<p> Aperiam urbanitas pri at, ad nec nonumy everti rationibus.</p>
									<p></p>	
								</td>
								<td class="cart_price">
									
								</td>
								<td class="cart_quantity">
									
								</td>
								<td class="cart_total">

								</td>
								<td class="cart_delete">

								</td>
							</tr>
							<tr>
								<td>
									<h4>SPECIFICATION</h4>
								</td>
								<td class="cart_description">
									<p>Material       :</p>
									<p>Panel    	  :</p>
									<p>Power   		  :</p>
									<p>Signal (input) :</p>
									<p>Power (output) :</p>
								</td>
								<td class="cart_description">
								</td>
								<td class="cart_price">
									<p></p>
								</td>
								<td class="cart_quantity">
										
								</td>
								<td class="cart_total">

								</td>
								<td class="cart_delete">

								</td>
							</tr>
							<tr>
								<td class="cart_product">
									<h4>WARRANTY</h4>
								</td>
								<td class="cart_description">
									<p>Lorem ipsum dolor sit amet, has putant ceteros mnesarchum eu.</p>
									<p> Aperiam urbanitas pri at, ad nec nonumy everti rationibus.</p>
									<p></p>	
								</td>
								<td class="cart_price">
										
								</td>
								<td class="cart_quantity">
										
								</td>
								<td class="cart_total">

								</td>
								<td class="cart_delete">

								</td>
							</tr>
							<tr>
								<td class="cart_product">
									<h4>ACCESSORIES INCLUDED</h4>
								</td>
								<td class="cart_description">
									<p>Lorem ipsum dolor sit amet, has putant ceteros mnesarchum eu.</p>
									<p> Aperiam urbanitas pri at, ad nec nonumy everti rationibus.</p>
									<p></p>														</td>
								<td class="cart_price">
										
								</td>
								<td class="cart_quantity">
										
								</td>
								<td class="cart_total">

								</td>
								<td class="cart_delete">

								</td>
							</tr>
						</table>
					</div>
					
					<div class="tab-pane fade" id="feature" >
						<div class="col-sm-12">
							<h4>SPECIAL FEATURES</h4>
							<hr>
						</div>
						<div class="col-sm-12">
							<h4>SOFTWARE COMPATIBILITY</h4>
							<hr>
						</div>
					</div>
					
					<div class="tab-pane fade active in" id="reviews" >
						<div class="table-responsive cart_info">
							<table class="table table-condensed">
								<thead>

								</thead>
								<tbody>
									<tr>
										<td class="cart_product">
											<h4>REVIEWS</h4>
											<p>there are no reviews yet</p>	
										</td>
									</tr>
									<tr>	
										<hr>
										<td class="cart_product">
											<h4>BE THE FIRST TO REVIEW LCD AD DISPLAY 43"</h4>
											<p>your email adress will not be published. required fields are marked* ☆☆☆☆☆</p>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
					
			<div class="recommended_items"><!--recommended_items-->
				<h2 class="title text-center">RELATED PRODUCTS</h2>
				<div id="recommended-item-carousel" class="carousel-recommend slide" data-ride="carousel">
			<center><div class="carousel-inner">
						<div class="item active">
							<div class="row">	
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-left">
												<img src="images/home/recommend1.jpg" alt=""  />
												<h4>LCD AD DISPLAY 22"</h4>
												<p>⭐⭐⭐⭐⭐</p>
												<p>IDR 9.028.800</p> <button type="button" class="btn btn-default add-to-cart" style="float:right;" ><i class="fa fa-shopping-cart" ></i>Add to cart</button>
											</div> 
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-left">
												<img src="images/home/recommend1.jpg" alt=""  />
												<h4>LCD AD DISPLAY 22"</h4>
												<p>⭐⭐⭐⭐⭐</p>
												<p>IDR 9.028.800</p> <button type="button" class="btn btn-default add-to-cart" style="float:right;" ><i class="fa fa-shopping-cart" ></i>Add to cart</button>
											</div> 
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-left">
												<img src="images/home/recommend1.jpg" alt=""  />
												<h4>LCD AD DISPLAY 22"</h4>
												<p>⭐⭐⭐⭐⭐</p>
												<p>IDR 9.028.800</p> <button type="button" class="btn btn-default add-to-cart" style="float:right;" ><i class="fa fa-shopping-cart" ></i>Add to cart</button>
											</div> 
										</div>
									</div>
								</div>
							</div>
						</div>
					</div></center>			
				</div>
			</div>
	
	
	
<!-- Modal content  1 -->
		
<!-- <center><div id="zoomImage" class="modal">
		<div class="modal-video">
			<span class="close_video">&times;</span>
			<div class="img-zoom-container">
				<img id="myimage" src="images/product-details/1.jpg" width="150" height="200">
				<div id="myresult" class="img-zoom-result"></div>
			</div>
		</div>
	</div></center> -->

<!-- Modal content  2 -->  

<div id="myModal" class="modal">
	<div class="modal-content  modal-lg">
		<button type="button" class="close" data-dissmiss="modal">&times;</button>
		<!-- <span class="close">&times;</span> -->
		<center><h2 style="color: #ffffff">Create Your Account</h2>
		<p>Already have an account?<a href="#">Login</a></p></center>
		<hr>
		<b>Email</b>
		<hr>
		<b>Username</b>
		<hr>
		<b>Password</b>
		<hr>
		<form action="/action_page.php">
			<input type="checkbox" name="vehicle" value="Bike"><p>Email me special offers and artist news.</p><br>
			<center><button id="myBtn" class="btn create">
			<div>
			CREATE MY ACCOUNT
			</div>
			</center>
		<p>by clicking create my account, you agree to our user agreement</b>

	</div>
</div>


    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/main.js"></script>
	<script>magnify("myimage", 3);</script> 