	
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
					<li><a href="index.php">Homepage</a></li>
					<li class="active">Services</li>
				</ol>
			</div>
			
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<div class="panel-group category-products" id="accordian">
							<!-- category-products -->
							<div class="panel panel-default">
								<div class="panel-heading">
										<center><h2>Category</h2></center>
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
											<span class="badge pull-right"><i class="fa fa-sort-down"></i></span>
											Holodisplay
										</a>
									</h4>
								</div>
								<div id="sportswear" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="">Hologram Display HD90 </a></li>
											<li><a href="">Hologram Display HD180 </a></li>
											<li><a href="">Hologram Display HD360 </a></li>
											<li><a href="">Virtual Presenter</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#mens">
											<span class="badge pull-right"><i class="fa fa-sort-down"></i></span>
											Digital Signage
										</a>
									</h4>
								</div>
								<div id="mens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="">Digital Ad Display</a></li>
											<li><a href="">Touchscreen Monitor</a></li>
											<li><a href="">Interactive Android Kiosk</a></li>
											<li><a href="">Smart Signage</a></li>
											<li><a href="">Mini PC Controller</a></li>
										</ul>
									</div>
								</div>
							</div>
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#womens">
											<span class="badge pull-right"><i class="fa fa-sort-down"></i></span>
											CATEGORY 3
										</a>
									</h4>
								</div>
								<div id="womens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="">Fendi</a></li>
											<li><a href="">Guess</a></li>
											<li><a href="">Valentino</a></li>
											<li><a href="">Dior</a></li>
											<li><a href="">Versace</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="product-details.php">Category 4</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="product-details.php">Category 5</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="product-details.php">Category 6</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="product-details.php">Category 7</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="product-details.php">Category 8</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="product-details.php">Category 9</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="product-details.php">Category 10</a></h4>
								</div>
							</div>
							<div class="panel panel-default" style="margin-left:20px;">
								<form action="/action_page.php" method="get">
									<input type="checkbox" style="margin-bottom:10px;">  Discount<br>
									<input type="checkbox" style="margin-bottom:10px;">  Pre Order<br>
									<input type="checkbox" style="margin-bottom:10px;">  Custom Design<br>
									<input type="submit" value="Submit">
								</form>
							</div>
											
										
						</div><!--/category-productsr-->						
					</div>
				</div>
				<div class="col-sm-9">
					<div class="features_items"><!--features_items-->
						<div class="row">
							<div class="col-sm-4 col-xs-6">
								<div class="product-image-wrapper">
									<div class="single-products">
										<div class="productinfo text-center">
											<img src="images/home/product1.jpg" alt="" />
											<h2>Product Name</h2>
											<p>⭐⭐⭐⭐⭐</p>
											<p>IDR 9.028.800</p>
											<a href="product-details.php" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<ul class="pagination">
							<li class="active"><a href="">1</a></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><a href="">&raquo;</a></li>
						</ul>
					</div><!--features_items-->
				</div>
