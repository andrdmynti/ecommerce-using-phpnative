<!-- jumbotron -->
	<div class="jumbotron text-center" style="background-image:url(./images/home/profe.jpg)">
		<h2 class="upperwhite">SERVICES</h2>	
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<p></p>
				</div>
				<div class="col-md-6">
					<div class="input-group custom-search-form">
						<input type="text" class="form-control">
						<button class="btn btn-primary" type="button">
						<span class="glyphicon glyphicon-search"></span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end jumbotron -->

	<!-- mega menu  -->
	
	<div class="category-tab">
		<center>
			<ul class="nav nav-tabs">
				<li class="dropdown">
					<div class="dropbtn">
						<a href="#" data-toggle="tab" style="padding-top:10px; padding-bottom:10px;">WEBSITE</a>
					</div>
					<div class="dropdown-content">
						<a href="#">Link 1</a>
						<a href="#">Link 2</a>
						<a href="#">Link 3</a>
					</div>
				</li>
				<li class="dropdown">
					<div class="dropbtn">
						<a href="#" data-toggle="tab" style="padding-top:10px; padding-bottom:10px;">3D FILES</a>
					</div>
					<div class="dropdown-content">
						<a href="#">Link 1</a>
						<a href="#">Link 2</a>
						<a href="#">Link 3</a>
					</div>
				</li>
				<li class="dropdown">
					<div class="dropbtn">
						<a href="#" data-toggle="tab" style="padding-top:10px; padding-bottom:10px;">VIDEO</a>
					</div>
					<div class="dropdown-content">
						<a href="#">Link 1</a>
						<a href="#">Link 2</a>
						<a href="#">Link 3</a>
					</div>
				</li>
				<li class="dropdown">
					<div class="dropbtn">
						<a href="#" data-toggle="tab" style="padding-top:10px; padding-bottom:10px;">AUDIO</a>
					</div>
					<div class="dropdown-content">
						<a href="#">Link 1</a>
						<a href="#">Link 2</a>
						<a href="#">Link 3</a>
					</div>
				</li>
				<li class="dropdown">
					<div class="dropbtn">
						<a href="#" data-toggle="tab" style="padding-top:10px; padding-bottom:10px;">GRAPHICS</a>
					</div>
					<div class="dropdown-content">
						<a href="#">Link 1</a>
						<a href="#">Link 2</a>
						<a href="#">Link 3</a>
					</div>
				</li>
				<li class="dropdown">
					<div class="dropbtn"><a href="#" data-toggle="tab" style="padding-top:10px; padding-bottom:10px;">PHOTOS</a></div>
					<div class="dropdown-content">
						<a href="#">Link 1</a>
						<a href="#">Link 2</a>
						<a href="#">Link 3</a>
					</div>
				</li>
				<li class="dropdown">
					<div class="dropbtn"><a href="#" data-toggle="tab" style="padding-top:10px; padding-bottom:10px;">APPS</a></div>
					<div class="dropdown-content">
						<a href="#">Link 1</a>
						<a href="#">Link 2</a>
						<a href="#">Link 3</a>
					</div>
				</li>
				<li class="dropdown">
					<div class="dropbtn"><a href="#" data-toggle="tab" style="padding-top:10px; padding-bottom:10px;">AR/VR</a></div>
					<div class="dropdown-content">
						<a href="#">Link 1</a>
						<a href="#">Link 2</a>
						<a href="#">Link 3</a>
					</div>
				</li>						
			</ul>
		</center>
	</div>