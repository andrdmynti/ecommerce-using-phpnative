<?php
	
	include '../action/connection.php';

	$id_category 	= $_GET['id'];

	$query 			= "SELECT * FROM product_category WHERE id='$id_category'";
	$hasil 			= mysqli_query($connect, $query)or die(mysqli_error());
	$tampil 		= mysqli_fetch_array($hasil);

?>
<div class="main-content-inner">
	<div class="breadcrumbs ace-save-state" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
				<i class="ace-icon fa fa-home home-icon"></i>
				<a href="index.php">Home</a>
			</li>
			<li>
				<a href="index.php?content=manage_category">Management Product Category</a>
			</li>
			<li class="active">Create New Category</li>
		</ul><!-- /.breadcrumb -->

		<div class="nav-search" id="nav-search">
			<form class="form-search">
				<span class="input-icon">
					<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
					<i class="ace-icon fa fa-search nav-search-icon"></i>
				</span>
			</form>
		</div><!-- /.nav-search -->
	</div>
	<br>
	<div class="page-content">
		<div class="page-header">
			<h1>
				Create New Product
			</h1>
		</div><!-- /.page-header -->
		<br>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<!-- PAGE CONTENT BEGINS -->
				<form class="form-horizontal" role="form" action="../action/doUpdateCategory.php" method="POST">
					<div class="form-group">
						<label class="col-sm-3" for="form-field-1">&nbsp;&nbsp;&nbsp;Product Category</label>
						<input name="id" value="<?php echo $tampil['id'] ?>">
						<div class="col-sm-9">
							<input type="text" id="form-field-1" name="category" value="<?php echo $tampil['category'] ?>" class="col-xs-10 col-sm-9" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10" id="default-buttons">
							<button type="submit" class="btn btn-primary btn-sm" style="float: right;">Update</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>