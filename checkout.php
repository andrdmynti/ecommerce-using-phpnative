<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Checkout | E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<link href="css/v.1.3.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<div class="site-wrapper-alternative">
		<div class="container-fluid" id="home">
			<nav class="navbar navbar-expand-lg navbar-dark">
				<ul class="navbar-brand ml-4">
					<li><a href="index.html"><img src="images/home/logo.png"></a></li>
				</ul>
			<!-- </div> -->
				<!-- <div class="navbar-header"> -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!-- </div> -->
				<div class="collapse navbar-collapse shop-menu" id="navbarNav">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<p></p>
						</li>
					</ul>
					<ul class="navbar-nav ">
						<li class="nav-item">
							<div class="searchbox input-group custom-search-form">
								<input type="text" class="form-control" style="">
								<span class="input-group-btn">
								<button class="btn btn-primary" type="button">
								<span class="glyphicon glyphicon-search"></span>
								</button>
								</span>
							</div>
						</li>
						<li class="nav-item" style="padding-left:150px; margin-top:12px;">
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-user"></i>My Account</button>
								<ul class="dropdown-menu">
									<li><a href="login.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Wishlist</p></a></li>
									<li><p></p></li>
									<li><a href="login.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Order & Status</p></a></li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li><a href="#">Shooping Cart</a></li>
				  <li class="active">Checkout</li>
				</ol>
			</div>
			<div>
				<center><h1>YOUR SHOPING CART</h1>
				<h3>Delivery & Payment</h3></center>
			</div>
		</div>		
	<hr>
	<div class="container">
		<div class="row">								
			<div class="col-sm-12">
				<div class="box-cart col-sm-7" style="padding-bottom:23px;">
					<div class="col-sm-1">
						<p></p>
					</div>
					<div class="col-sm-4">
						<h5>Total</h5>
					</div>
					<div class="col-sm-3">
						<p></p>
					</div>
					<div class="col-sm-4">
						<h3><small>IDR</small> 60.000.000</h3>
					</div>
				</div>
				
				<div class="box col-sm-4 ">
					<p>Order ID:<b>2018040371243</b></p><p>Total Payable</p>
				</div>			
			</div>
		</div>
		<div class="row">								
			<div class="col-sm-12">
				<div class="box-cart col-sm-7"  style="padding-bottom:3px;">
					<div class="col-sm-1">
						<p></p>
					</div>
					<div class="col-sm-4">
						<h5>Order ID</h5>
					</div>
					<div class="col-sm-3">
						<p></p>
					</div>
					<div class="col-sm-4">
						<h4>71241</h4>
					</div>
				</div>
				
				<div class="box col-sm-4 ">
					<h3>IDR 60.000.000</h3>
				</div>			
			</div>
		</div>
		<div class="row">								
			<div class="col-sm-12">
				<div class="box-cart col-sm-7" style="padding-bottom:5px;">
					<div class="col-sm-1">
						<p></p>
					</div>
					<div class="col-sm-4">
						<h5>Product</h5>
					</div>
					<div class="col-sm-3">
						<p></p>
					</div>
					<div class="col-sm-4">
						<h5>Order Total</h5>
					</div>
				</div>
				
				<div class="box col-sm-4 ">
					<p>Please make a payment to the bank bellow</p>
				</div>			
			</div>
		</div>
		<div class="row">								
			<div class="col-sm-12">

					<div class="box-cart col-sm-7">
						<div class="row">
							<div class="col-sm-1">
								<p></p>
							</div>
							<div class="col-sm-4">
								<h5>Android Kiosk 55"</h5>
							</div>
							<div class="col-sm-3">
								<p></p>
							</div>
							<div class="col-sm-4">
								<h5>30.000.000</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-1">
								<p></p>
							</div>
							<div class="col-sm-4">
								<h5>LCD cover 42"</h5>
							</div>
							<div class="col-sm-3">
								<p></p>
							</div>
							<div class="col-sm-4">
								<h5>30.000.000</h5>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-1">
								<p></p>
							</div>
							<div class="col-sm-4">
								<b>Name</b>
							</div>
							<div class="col-sm-3">
								<p></p>
							</div>
							<div class="col-sm-4">
								<b>Address</b>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-1">
								<p></p>
							</div>
							<div class="col-sm-4">
								<h5>Sri Dewi Putri</h5>
							</div>
							<div class="col-sm-3">
								<p></p>
							</div>
							<div class="col-sm-4">
								<h5>Jl. Duren Sawit K1/10 Jakarta Timur 13340</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-1">
								<p></p>
							</div>
							<div class="col-sm-4">
								<b>Mobile</b>
							</div>
							<div class="col-sm-3">
								<p></p>
							</div>
							<div class="col-sm-4">
								<p></p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-1">
								<p></p>
							</div>
							<div class="col-sm-4">
								<p>+62 87881055205</p>
							</div>
							<div class="col-sm-3">
								<p></p>
							</div>
							<div class="col-sm-4">
								<p></p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-1">
								<p></p>
							</div>
							<div class="col-sm-4">
								<b>Email</b>
							</div>
							<div class="col-sm-3">
								<p></p>
							</div>
							<div class="col-sm-4">
								<p></p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-1">
								<p></p>
							</div>
							<div class="col-sm-4">
								<p>Sri.dewi@holomoc.com</p>
							</div>
							<div class="col-sm-3">
								<p></p>
							</div>
							<div class="col-sm-4">
								<p></p>
							</div>
						</div>
							<!-- SAVE BUTTON -->
						<div class="row">								
							<center><div><input type="submit" value="Save" class="btn btn-primary btn-block btn-lg" tabindex="7" style="margin-left:50px; margin-bottom:50px;"></div></center>
							<br>
						</div>
					</div>
				
				
				
				<div class="box col-sm-4 ">
					<div class="row">
						<div class="col-sm-12">
							<center><img src="images/cart/bca.png" height="50" width="120">
							<p>BCA Bank Branch Duren Sawit</p></center>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<h5>A/n PT Holomoc Indonesia</h5>
							<h5>Account No:0063977338</h5>
						</div>
					</div>
					<div class="box-dark row">
						<div class="col-sm-12">
							<p>Make sure you pay <b>exactly the total bill you get.</b>an incorrect amount will prevent us from performing payment verivications</p>
							<p>Confirmation of <b>PAYMENT IS NOT NECESSARY</b>as long as you make payment according to the numbers mentioned</p>
							<p>We will cancel your order if within 1 x 24 hours no payment.</p>
						</div>
					</div>
					<div class="row">
						<center><smal style="margin-left:35px; margin-bottom:10px;">Need help? Contact Us (021)2265 7281 or email info@holomoc.com</smal></center>
					</div>
					<div class="row">
						<center><div class="col-xs-12 col-md-12"><input type="submit" value="Back to Shop" class="btn btn-primary btn-block btn-lg" tabindex="7" style="margin-bottom: 0px;"></div></center>
					</div>
				</div>			
			</div>
		</div>
	</div>
	</section> <!--/#cart_items-->

	<!-- Modal -->
	<div class="modal fade" id="t_and_c_m" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
				</div>
				<div class="modal-body">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
				</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">I Agree</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</div>

	<footer id="footer"><!--Footer-->
		<div class="footer-bottom">
			<div class="container">
			<div class="row">
					<p class="pull-left">Copyright © 2018 HOLOMOC. All rights reserved.</p>
					<p class="pull-right">Need help? <span><a target="_blank" href="http://www.themeum.com">Contact Us?</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->



    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>
