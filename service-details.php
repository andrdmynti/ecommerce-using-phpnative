<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Service Details | E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header-middle">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.html"><img src="images/home/logo.png" alt="" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li><div class="search_box pull-right">
									<input type="text" placeholder="Search"/>
									</div></li>
								<li><a href="shop.html"><i class="fa fa-crosshairs"></i> Shop</a></li>
								<li><a href="Service.html"><i class="fa fa-pencil-square-o"></i> Service</a></li>
								<li><div class="panel-akun panel-default">
										<div class="dropdown">
											<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-user"></i>My Account
											<span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a href="wishlist.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Wishlist</p></a></li>
												<li><p></p></li>
												<li><a href="order.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Order & Status</p></a></li>
											</ul>
									</div> 
									</div></li>
								<li><a href="cart.html"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header><!--/header-->
	
	<section>
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
					<li><a href="#">Services</a></li>
					<li class="active">Web Design</li>
				</ol>
			</div>
			<div class="row">								
				<div class="col-sm-12">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-7">
							<div class="view-product">
								<img src="images/home/gallery1.jpg" style="height:435px; widht: 300px;"/>
							</div>
							<div id="similar-product" class="carousel slide" data-ride="carousel">
								
								  <!-- Wrapper for slides -->
								  <!-- Controls -->
								  <a class="left item-control" href="#similar-product" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								  </a>
								  <a class="right item-control" href="#similar-product" data-slide="next">
									<i class="fa fa-angle-right"></i>
								  </a>
							</div>

						</div>
						<div class="col-sm-5">
						<!--/product-information-->
							<div class="product-information">
								<img src="images/product-details/new.jpg" class="newarrival" alt="" />
								<h1>Website Design</h1>
								<p>Category: Services</p>
								<img src="images/product-details/rating.png" alt="" />
								<span>90 reviews</span>
							</div>
							<div class="product-information" style="background-color:rgb(231, 239, 241)">
								<label>Description:</label>
								<p>.</p>
								<p>.</p>
								<p>.</p>
								<p>.</p>
								<p>.</p>
							</div>
							<div class="product-information" style="background-color:rgb(231, 239, 241)">
								<button type="button" class="btn btn-fefault cart">
								<div>
														Add to cart
								</div>
								</button>
								<button id="myBtn" class="btn btn-fefault cart">
								<div>
														Buy Now
								</div>
								</button>
									</div>
								</div>
									<div id="myModal" class="modal">
										<div class="modal-content  modal-lg">
											<span class="close">&times;</span>
											<center><h2 style="color: #ffffff">Create Your Account</h2>
											<p>Already have an account?<a href="#">Login</a></p></center>
											<hr>
											<b>Email</b>
											<hr>
											<b>Username</b>
											<hr>
											<b>Password</b>
											<hr>
											<form action="/action_page.php">
												<input type="checkbox" name="vehicle" value="Bike"><p>Email me special offers and artist news.</p><br>
												<center><button id="myBtn" class="btn create">
												<div>
												CREATE MY ACCOUNT
												</div>
												</center>
											<b>by clicking create my account, you agree to our user agreement</b>
										</div>
									</div>
								<a href=""><img src="images/product-details/share.png" class="share img-responsive"  alt="" /></a>
							</div>
						</div>
						<!--/product-information-->
					</div><!--/product-details-->
					<hr>
					<div class="row"><!--FEATURENYA-->
						<div class="col-sm-4">
							<p></p>
						</div>
						<div class="col-sm-4">
							<center><h4>FEATURES</h4></center>
						</div>
					</div>
					<div class="row" style="margin-top: 50px;">
						<div class="col-sm-1">
							<p></p>
						</div>
						<div class="col-sm-3">
							<img src="./images/Icon/Responsive.png" height="100px" width="100px" style="float:left;">
							<h5>Responsive Design</h5>
							<p>Looks great on all Devices</p>
						</div>
						<div class="col-sm-1">
							<p></p>
						</div>
						<div class="col-sm-3">
							<img src="./images/Icon/Sosial Media.png" height="100px" width="100px" style="float:left;">
							<h5>Social Media</h5>
							<p>Integrate all your accounts	</p>
						</div>
						<div class="col-sm-1">
							<p></p>
						</div>
						<div class="col-sm-3">
							<img src="./images/Icon/SEO.png" height="100px" width="100px" style="float:left;">
							<h5>SEO</h5>
							<p>Optimized for search engine</p>						
						</div>
					</div>
					<div class="row" style="margin-top: 50px;">
						<div class="col-sm-1">
							<p></p>
						</div>
						<div class="col-sm-3">
							<img src="./images/Icon/Plugin.png" height="100px" width="100px" style="float:left;">
							<h5>Plugins</h5>
							<p>Supercharge your website</p>
						</div>
						<div class="col-sm-1">
							<p></p>
						</div>
						<div class="col-sm-3">
							<img src="./images/Icon/E-Commerce.png" height="100px" width="100px" style="float:left;">
							<h5>E-Commerce</h5>
							<p>Sell your product for service online</p>
						</div>
						<div class="col-sm-1">
							<p></p>
						</div>
						<div class="col-sm-3">
							<img src="./images/Icon/Blog.png" height="100px" width="100px" style="float:left;">
							<h5>Blog</h5>
							<p>Tell the world your story</p>						
						</div>
					</div>
					<div class="row" style="margin-top: 50px;">
						<div class="col-sm-1">
							<p></p>
						</div>
						<div class="col-sm-3">
							<img src="./images/Icon/Page.png" height="100px" width="100px" style="float:left;">
							<h5>Pages</h5>
							<p>Showcase your product and services</p>
						</div>
						<div class="col-sm-1">
							<p></p>
						</div>
						<div class="col-sm-3">
							<img src="./images/Icon/Contact Forms.png" height="100px" width="100px" style="float:left;">
							<h5>Contact forms</h5>
							<p>Generate Enquire and get feedback</p>
						</div>
						<div class="col-sm-1">
							<p></p>
						</div>
						<div class="col-sm-3">
							<img src="./images/Icon/Page Builder.png" height="100px" width="100px" style="float:left;">
							<h5>Page Builder</h5>
							<p>Build impresive landing pages</p>						
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<footer id="footer"><!--Footer-->
		
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2018 HOLOMOC. All rights reserved.</p>
					<p class="pull-right">Need Help?<span><a target="_blank" href="#">Contact Us</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>