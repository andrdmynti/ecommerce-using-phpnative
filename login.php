<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login | E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<link href="css/v.1.3.css" rel="stylesheet">

     
</head><!--/head-->

<body style="background-image:url(images/bg_login.jpg)">			
	<div class="container">
		<div class="row">
			<div class="col-md-1">
				<p>

				</p>
			</div>
			<div class="col-md-4">
				<div class="user_opsi">
					<h2 class="form_title">Login</h2>
					<form class="forms">
						<fieldset class="email_password">
							<div class="form_field">
								<input class="field_input" placeholder="email" required="" autofocus="" type="email">
							</div>
							<div class="form_field">
								<input class="field_input" placeholder="password" required="" autofocus="" type="password">
							</div>
							<div class="forms_buttons">
								<button class="forgot_password_button" type="button">Forgot password?</button>
								<input class="login_buton" value="Log In" type="submit">
							</div>
						</fieldset>
					</form>
				</div>
			</div>
			<div class="col-md-2">
				<p>

				</p>
			</div>
			<div class="col-md-4">
				<div class="register_opsi">
					<h2 class="form_title">Sign Up</h2>
					<form class="forms">
						<fieldset class="email_password">
							<div class="form_field">
								<input class="field_input" placeholder="full_name" required="" autofocus="" type="full_name">
							</div>
							<div class="form_field">
								<input class="field_input" placeholder="email" required="" autofocus="" type="email">
							</div>
							<div class="form_field">
								<input class="field_input" placeholder="password" required="" autofocus="" type="password">
							</div>

							<div class="forms_buttons">
								<input class="register_buton" value="register" type="submit">
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
  
</body>
</html>