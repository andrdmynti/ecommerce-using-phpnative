<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cart | E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<link href="css/v.1.3.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<div class="site-wrapper-alternative">
		<div class="container-fluid" id="home">
			<nav class="navbar navbar-expand-lg navbar-dark">
				<ul class="navbar-brand ml-4">
					<li><a href="index.html"><img src="images/home/logo.png"></a></li>
				</ul>
			<!-- </div> -->
				<!-- <div class="navbar-header"> -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!-- </div> -->
				<div class="collapse navbar-collapse shop-menu" id="navbarNav">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<p></p>
						</li>
					</ul>
					<ul class="navbar-nav ">
						<li class="nav-item">
							<div class="searchbox input-group custom-search-form">
								<input type="text" class="form-control" style="">
								<span class="input-group-btn">
								<button class="btn btn-primary" type="button">
								<span class="glyphicon glyphicon-search"></span>
								</button>
								</span>
							</div>
						</li>
						<li class="nav-item" style="padding-left:150px; margin-top:12px;">
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-user"></i>My Account</button>
								<ul class="dropdown-menu">
									<li><a href="login.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Wishlist</p></a></li>
									<li><p></p></li>
									<li><a href="login.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Order & Status</p></a></li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
	<!-- <header id="header">
		<div class="header-middle">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo">
							<a href="index.html"><img src="images/home/logo.png" class=" pull-left" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="col-lg-4">
							<div class="searchbox input-group custom-search-form">
								<input type="text" class="form-control" style="">
								<span class="input-group-btn">
								<button class="btn btn-primary" type="button">
								<span class="glyphicon glyphicon-search"></span>
								</button>
								</span>
							</div>
						</div>
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav pull-right">
								<li><div class="panel-akun panel-default">
										<div class="dropdown">
											<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-user"></i>My Account
											<span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a href="wishlist.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Wishlist</p></a></li>
												<li><p></p></li>
												<li><a href="order.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Order & Status</p></a></li>
											</ul>
									</div> 
									</div></li>
								<li><a href="cart.html"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header> -->

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs" style="margin-top:85px;">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div>
				<center><h1>YOUR SHOPING CART</h1>
				<h3>Delivery & Payment</h3></center>
			</div>
		</div>		
	<hr>
	<div class="container">
		<div class="row">								
			<div class="col-sm-12">
				<div class="box-left col-sm-7">
					<div class=" col-sm-8 col-md-6 col-sm--2">
						<form role="form">
							<hr>
							<h2><small>Add new address</small></h2>
							<hr>
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" id="first_name" class="form-control input-lg" placeholder="First Name" tabindex="1">
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text"  id="last_name" class="form-control input-lg" placeholder="Last Name" tabindex="2">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" id="post_code" class="form-control input-lg" placeholder="Post Code" tabindex="3">
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text"  id="Town/Locality" class="form-control input-lg" placeholder="Last Name" tabindex="4">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" id="City_name" class="form-control input-lg" placeholder="City" tabindex="3">
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text"  id="State" class="form-control input-lg" placeholder="State" tabindex="4">
									</div>
								</div>
							</div>
							<div class="form-group">
								<input type="email" id="Address" class="form-control input-lg" placeholder="Address" tabindex="5" style="height:50px;">
							</div>
							<div class="form-group">
								<input type="text"  id="Mobile" class="form-control input-lg" placeholder="Mobile No." tabindex="6">
							</div>
							<div style="margin-bottom:250px;">						
								<input type="submit" value="Save" class="btn btn-primary btn-block btn-lg" tabindex="7">
							</div>
						</form>
							<!-- SAVE BUTTON -->
					</div>
				</div>


				<div class="box col-sm-4 ">
				
					<div class="table-responsive cart_info">
						<table class="table table-condensed">
							<thead>
								<tr class="cart_menu">
									<td class="image">Item</td>
									<td class="description"></td>
									<td class="price">Price</td>
									<td></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="cart_product">
										<a href=""><img src="images/cart/one.png" alt=""></a>
									</td>
									<td class="cart_description">
										<h5><a href="">LCD Cover 42"</a></h5>
										<div class="cart_quantity_button">
											<a class="cart_quantity_up" href=""> - </a>
											<input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
											<a class="cart_quantity_down" href=""> + </a>
										</div>
									</td>
									<td class="cart_price">
										<p>IDR 20.000.000</p>
									</td>									<td class="cart_delete">
										<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
									</td>
								</tr>

								<tr>
									<td class="cart_product">
										<a href=""><img src="images/cart/one.png" alt=""></a>
									</td>
									<td class="cart_description">
										<h5><a href="">LCD Cover 42"</a></h5>
										<div class="cart_quantity_button">
											<a class="cart_quantity_up" href=""> - </a>
											<input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
											<a class="cart_quantity_down" href=""> + </a>
										</div>
									</td>
									<td class="cart_price">
										<p>IDR 20.000.000</p>
									</td>	
									<td class="cart_delete">
										<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
									</td>
								</tr>

								<tr>
									<td class="cart_product">
										<a href=""><img src="images/cart/one.png" alt=""></a>
									</td>
									<td class="cart_description">
										<h5><a href="">LCD Cover 42"</a></h5>
										<div class="cart_quantity_button">
											<a class="cart_quantity_up" href=""> - </a>
											<input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
											<a class="cart_quantity_down" href=""> + </a>
										</div>
									</td>
									<td class="cart_price">
										<p>IDR 20.000.000</p>
									</td>	
									<td class="cart_delete">
										<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
									</td>
								</tr>
							</tbody>
						</table>
					<div class="row">
						<br>
						<div class="col-sm-4">
							<b>Order Total</b>
						</div>
						<br>
						<div class="col-sm-4 offset-sm-4">
							<b>IDR 60.000.000</b>
						</div>
					</div>
					<div class="row">
						<br>
						<div class="col-sm-4">
							<p>Delivery</p>
						</div>
						<br>
						<div class="col-sm-4 offset-sm-4">
							<p>Menunggu Alamat</p>
						</div>
					</div>
					<hr style="color:#33a4c0">
					<div class="row">
						<br>
						<div class="col-sm-12">
							<h4>Total Payable</h4>
						</div>
						<br>
						<div class="col-sm-6">
							<h4><b>IDR 60.000.000</b></h4>
						</div>
					</div>
				<div>
					<center><div class="col-xs-12 col-md-12" style="margin-bottom:10px; margin-top:10px;" ><input type="submit" value="Checkout" class="btn btn-primary btn-block btn-lg" tabindex="7"></div></center>
				</div>
			</div>
		</div>

				<div class="box-left col-sm-7">
					<!-- payment method -->
					<div style="width:100%;">
						<hr>
						<h3>Payment Method</h3>
						<hr>
						<table class="table">
							<th>
								<center><img src="images/cart/bca1.png" width="100%"></center>
								<center><p style="font-size:10px">Transfer BCA Otomatis</p></center>
								<center><input type="radio" name="gender"></center>
							</th>
							<th>
								<center><img src="images/cart/visa1.png" width="100%"></center>
								<center><p style="font-size:10px">Kartu Kredit</p></center>
								<center><input type="radio" name="gender"></center>
							</th>
							<th>
								<center><img src="images/cart/tf1.png" width="100%"></center>
								<center><p style="font-size:10px">Transfer Bank</p></center>
								<center><input type="radio" name="gender"></center>
							</th>
							<th>
								<center><img src="images/cart/mandiri ecash1.png" width="100%"></center>
								<center><p style="font-size:10px">Mandiri E-cash</p></center>
								<center><input type="radio" name="gender"></center>
							</th>
							<th>
								<center><img src="images/cart/cod1.png" width="100%"></center>
								<center><p style="font-size:10px">COD</p></center>
								<center><input type="radio" name="gender"></center>
							</th>
						<table>
						<!-- <div class="row">
							<div class="col-md-2 col-md-offset-1">
								<center><img src="images/cart/bca1.png" width="100%"></center>
								<center><p style="font-size:10px">Transfer BCA Otomatis</p></center>
								<center><input type="radio" name="gender"></center>
							</div>
							<div class="col-md-2">
								<center><img src="images/cart/visa1.png" width="100%"></center>
								<center><p style="font-size:10px">Kartu Kredit</p></center>
								<center><input type="radio" name="gender"></center>
							</div>
							<div class="col-md-2">
								<center><img src="images/cart/tf1.png" width="100%"></center>
								<center><p style="font-size:10px">Transfer Bank</p></center>
								<center><input type="radio" name="gender"></center>
							</div>
							<div class="col-md-2">
								<center><img src="images/cart/mandiri ecash1.png" width="100%"></center>
								<center><p style="font-size:10px">Mandiri E-cash</p></center>
								<center><input type="radio" name="gender"></center>
							</div>
							<div class="col-md-2">
								<center><img src="images/cart/cod1.png" width="100%"></center>
								<center><p style="font-size:10px">COD</p></center>
								<center><input type="radio" name="gender"></center>
							</div>
						</div> -->
					</div>
				</div>	
			</div>
		</div>
	</div>
	</section> <!--/#cart_items-->

	
	<!-- end -->
	
	<!-- Modal -->
	<div class="modal fade" id="t_and_c_m" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
				</div>
				<div class="modal-body">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
				</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">I Agree</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</div>
	

	<footer id="footer"><!--Footer-->

		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2018 HOLOMOC. All rights reserved.</p>
					<p class="pull-right">Need Help?<span><a target="_blank" href="contact-us.html">Contact Us</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->



    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>
