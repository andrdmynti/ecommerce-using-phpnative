<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div class="left-sidebar">
				<h2>PRODUCTS</h2>				
			</div>
		</div>
	</div>
	
	
	<div class="row">
		<div class="col-sm-12">
			<div class="features_items"><!--features_items-->
				<?php
					include 'action/connection.php';

					$query 		= "SELECT * FROM product ORDER BY product.id DESC LIMIT 12";
					$insert	 	= mysqli_query($connect,$query);
					while ($tampil = mysqli_fetch_array($insert)) { ?>
					<div class="col-md-3">
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img src="images/<?php echo $tampil['product_image'] ?>" alt="<?php echo $tampil['product_name'] ?>">
									<br><br>
									<h2><?php echo $tampil['product_name'] ?></h2>
									<p>⭐⭐⭐⭐⭐</p>
									<p>IDR 9.028.800</p>
									<a href="index.php?content=product_details&&id=<?php echo $tampil['id'] ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
								</div>
							</div>
						</div>
					</div>
					<?php
						}
					?>
			</div>
		</div>
	</div>

	<!-- see more -->
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
					<div class="features_items"><!--features_items-->
						<div class="col-sm-5">
						<p></p>
						</div>
						<div class="col-sm-2">
								<a href="shop.php"><center><button class="btn btn-default get" type="button"> SEE ALL PRODUCTS</button></center></a>
						</div>
					</div>
			</div>
		</div>
	</div>	
	<!-- end see more -->
			
			<div class="category-tab"><!--category-tab-->
				<div class="row">
					<div class="col-sm-12">
						<div class="left-sidebar" style="margin-top:30px;">
							<h2>PORTOFOLIO</h2>				
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="right-sidebar">
							<a href="portofolio.php">Read More</a>				
						</div>
					</div>
				</div>
				<div class="tab-content">
					<div class="row">
						<div class="col-sm-3">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
										<h5>Virtual Reality Space Shuttle</h5>
										<p>oct 25 2017</p>
										<p>Skyworld, TMII</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
										<h5>Augmented Reality Monster Launching Game Asmaul Husna</h5>
										<p>oct 25 2017</p>
										<p>Skyworld, TMII</p>
									</div>
									
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
										<h5>Hologram Display HD 180</h5>
										<p>oct 25 2017</p>
										<p>ASTRA International</p>
									</div>
									
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
										<h5>Hologram Display HD 180</h5>
										<p>oct 25 2017</p>
										<p>ASTRA International</p>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!--/category-tab-->
			
			
			
		</div>
	</div>
</div>