<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Contact | E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDCJ6cZ8t_HBdb2439Chkitvr162RbJ3h4&callback=initMap" type="text/javascript"></script>
	<!-- <script>
		function myMap() {
		  var mapOptions1 = {
			center: new google.maps.LatLng(51.508742,-0.120850),
			zoom:9,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		  };
		  var mapOptions2 = {
			center: new google.maps.LatLng(51.508742,-0.120850),
			zoom:9,
			mapTypeId: google.maps.MapTypeId.SATELLITE
		  };
		  var mapOptions3 = {
			center: new google.maps.LatLng(51.508742,-0.120850),
			zoom:9,
			mapTypeId: google.maps.MapTypeId.HYBRID
		  };
		  var mapOptions4 = {
			center: new google.maps.LatLng(51.508742,-0.120850),
			zoom:9,
			mapTypeId: google.maps.MapTypeId.TERRAIN
		  };
		  var map1 = new google.maps.Map(document.getElementById("googleMap1"),mapOptions1);
		  var map2 = new google.maps.Map(document.getElementById("googleMap2"),mapOptions2);
		  var map3 = new google.maps.Map(document.getElementById("googleMap3"),mapOptions3);
		  var map4 = new google.maps.Map(document.getElementById("googleMap4"),mapOptions4);
		}
	</script> -->
	<!-- <script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>  -->
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header-middle">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.html"><img src="images/home/logo.png" alt="" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li><div class="search_box pull-right">
									<input type="text" placeholder="Search"/>
									</div></li>
								<li><a href="shop.html"><i class="fa fa-crosshairs"></i> Shop</a></li>
								<li><a href="services.html"><i class="fa fa-pencil-square-o"></i> Service</a></li>
								<li><div class="panel-akun panel-default">
										<div class="dropdown">
											<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-user"></i>My Account
											<span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a href="login.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Wishlist</p></a></li>
												<li><p></p></li>
												<li><a href="login.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Order & Status</p></a></li>
											</ul>
									 	</div>
									</div></li>
								<li><a href="cart.html"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header><!--/header-->

	<div class="row">
		<div class="col-md-12">
			<div id="map">
				<center>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.2982976743383!2d106.90967831446659!3d-6.224342995494304!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f335951ad791%3A0x587b6a48f6b456ea!2sHolomoc!5e0!3m2!1sen!2sid!4v1530612100302" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</center>
				</div>
		</div>
	</div>

	<br>

	<div class="row">
		<div class="col-md-1">
			<p>
				
			</p>
		</div>
		<div class="col-md-3">
			<h4>OFFICE</h4>
		</div>
		<div class="col-md-2">
			<p>

			</p>
		</div>
		<div class="col-md-5">
			<h4>SUBMIT A QUESTION</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-md-1">
			<p>
				
			</p>
		</div>
		<div class="col-md-3">
			<p>Jalan Duren Sawit Indah Blok K1/10</p>
			<p>Duren Sawit, Jakarta Timur 13440</p>
			<br>
			<p>Work : (021)22857281</p>
			<p>Mobile : 0812-8523-2001</p>
		</div>
		<div class="col-md-2">
			<p>

			</p>
		</div>
		<div class="col-md-5">
			<p>Please note we're experiencing high volume of support requests.<br>
				We will get back to you within 1-2 business day.<br>
				If you do not hear back from us within 3 business days, please check<br>
				your spam folder as our reply may have been sent there by mistake.</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-1">
			<p>

			</p>
		</div>
		<div class="col-md-3">
			<div class="col-md-6">
				<center><input type="Get Direction" value="Get Direction" class="btn btn-primary btn-block btn-lg" tabindex="7"></center>
			</div>
		</div>
		<div class="col-md-2">
			<p>

			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<input type="Name" name="Name" id="email" class="form-control-contact input-lg" placeholder="Name" tabindex="4">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-1">
			<p>

			</p>
		</div>
		<div class="col-md-3">
		<p>	</p>
		</div>
		<div class="col-md-2">
			<p>

			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<input type="email" name="email" id="email" class="form-control-contact input-lg" placeholder="Email" tabindex="4">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-1">
			<p>

			</p>
		</div>
		<div class="col-md-3">
		<p>	</p>
		</div>
		<div class="col-md-2">
			<p>

			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<input type="Subject" name="Subject" id="email" class="form-control-contact input-lg" placeholder="Subject" tabindex="4">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-1">
			<p>

			</p>
		</div>
		<div class="col-md-3">
		<p>	</p>
		</div>
		<div class="col-md-2">
			<p>

			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<textarea type="Message" name="Message" id="email" class="form-control-contact input-message" placeholder="Message" tabindex="4"></textarea>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-1">
			<p>

			</p>
		</div>
		<div class="col-md-3">
		<p>	</p>
		</div>
		<div class="col-md-2">
			<p>

			</p>
		</div>
		<div class="col-md-6">
			<div class="col-md-6">
				<center><input type="submit" value="submit" class="btn btn-primary btn-block btn-lg" tabindex="7"></center>
			</div>
		</div>
	</div>
	
	<footer id="footer"><!--Footer-->
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2018 HOLOMOC. All rights reserved.</p>
					<p class="pull-right">Need Help?<span><a target="_blank" href="contact-us.html">Contact Us</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	

  
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="js/gmaps.js"></script>
	<script src="js/contact.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/main.js"></script>
</body>
</html>