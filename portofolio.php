<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Holomoc </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header-middle">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.html"><img src="images/home/logo.png" alt="" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li><div class="search_box pull-right">
									<input type="text" placeholder="Search"/>
									</div></li>
								<li><a href="shop.html"><i class="fa fa-crosshairs"></i> Shop</a></li>
								<li><a href="services.html"><i class="fa fa-pencil-square-o"></i> Service</a></li>
								<li><div class="panel-akun panel-default">
										<div class="dropdown">
											<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-user"></i>My Account
											<span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a href="login.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Wishlist</p></a></li>
												<li><p></p></li>
												<li><a href="login.html" style="background-color: rgb(255, 255, 255);"> <p style="color:rgb(92, 92, 92)">Order & Status</p></a></li>
											</ul>
									 	</div>
									</div></li>
								<li><a href="cart.html"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header><!--/header-->

<div class="category-tab"><!--category-tab-->
    <div class="col-sm-12">
        <div class="col-sm-4">
            <p>

            </p>
        </div>
        <div class="col-sm-4">
            <center><h2>PORTOFOLIO</h2></center>
            <br>		
            <br>
            <br>		
            <br>
        </div>
    </div>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="tshirt">
            <div class="row">
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
                                <h5>Virtual Reality Space Shuttle</h5>
                                <p>oct 25 2017</p>
                                <p>Skyworld, TMII</p>
                                <br>
                                <p align="justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolorem dolor fugit repellat cupiditate totam nihil accusantium dicta, quia veniam saepe illum debitis nisi maiores facilis iste omnis obcaecati. Velit, illum.</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">


                                <button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
                                <h5>Augmented Reality Monster
                                    <br>   
                                Launching Game Asmaul Husna</h5>
                                <p>oct 25 2017</p>
                                <p>Skyworld, TMII</p>
                                <br>
                                <p align="justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolorem dolor fugit repellat cupiditate totam nihil accusantium dicta, quia veniam saepe illum debitis nisi maiores.</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
                                <h5>Hologram Display HD 180</h5>
                                <p>oct 25 2017</p>
                                <p>ASTRA International</p>
                                <br>
                                <p align="justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolorem dolor fugit repellat cupiditate totam nihil accusantium dicta, quia veniam saepe illum debitis nisi maiores facilis iste omnis obcaecati. Velit, illum.</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
                                <h5>Hologram Display HD 180</h5>
                                <p>oct 25 2017</p>
                                <p>ASTRA International</p>
                                <br>
                                <p align="justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolorem dolor fugit repellat cupiditate totam nihil accusantium dicta, quia veniam saepe illum debitis nisi maiores facilis iste omnis obcaecati. Velit, illum.</p>
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
                                <h5>Virtual Reality Space Shuttle</h5>
                                <p>oct 25 2017</p>
                                <p>Skyworld, TMII</p>
                                <br>
                                <p align="justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolorem dolor fugit repellat cupiditate totam nihil accusantium dicta, quia veniam saepe illum debitis nisi maiores facilis iste omnis obcaecati. Velit, illum.</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">


                                <button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
                                <h5>Augmented Reality Monster
                                    <br>   
                                Launching Game Asmaul Husna</h5>
                                <p>oct 25 2017</p>
                                <p>Skyworld, TMII</p>
                                <br>
                                <p align="justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolorem dolor fugit repellat cupiditate totam nihil accusantium dicta, quia veniam saepe illum debitis nisi maiores.</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
                                <h5>Hologram Display HD 180</h5>
                                <p>oct 25 2017</p>
                                <p>ASTRA International</p>
                                <br>
                                <p align="justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolorem dolor fugit repellat cupiditate totam nihil accusantium dicta, quia veniam saepe illum debitis nisi maiores facilis iste omnis obcaecati. Velit, illum.</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
                                <h5>Hologram Display HD 180</h5>
                                <p>oct 25 2017</p>
                                <p>ASTRA International</p>
                                <br>
                                <p align="justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolorem dolor fugit repellat cupiditate totam nihil accusantium dicta, quia veniam saepe illum debitis nisi maiores facilis iste omnis obcaecati. Velit, illum.</p>
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
                                <h5>Virtual Reality Space Shuttle</h5>
                                <p>oct 25 2017</p>
                                <p>Skyworld, TMII</p>
                                <br>
                                <p align="justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolorem dolor fugit repellat cupiditate totam nihil accusantium dicta, quia veniam saepe illum debitis nisi maiores facilis iste omnis obcaecati. Velit, illum.</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">


                                <button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
                                <h5>Augmented Reality Monster
                                    <br>   
                                Launching Game Asmaul Husna</h5>
                                <p>oct 25 2017</p>
                                <p>Skyworld, TMII</p>
                                <br>
                                <p align="justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolorem dolor fugit repellat cupiditate totam nihil accusantium dicta, quia veniam saepe illum debitis nisi maiores.</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
                                <h5>Hologram Display HD 180</h5>
                                <p>oct 25 2017</p>
                                <p>ASTRA International</p>
                                <br>
                                <p align="justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolorem dolor fugit repellat cupiditate totam nihil accusantium dicta, quia veniam saepe illum debitis nisi maiores facilis iste omnis obcaecati. Velit, illum.</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <button id="myBtn"><img src="images/home/gallery1.jpg" alt="" /></button>
                                <h5>Hologram Display HD 180</h5>
                                <p>oct 25 2017</p>
                                <p>ASTRA International</p>
                                <br>
                                <p align="justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolorem dolor fugit repellat cupiditate totam nihil accusantium dicta, quia veniam saepe illum debitis nisi maiores facilis iste omnis obcaecati. Velit, illum.</p>
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!--/category-tab-->

<footer id="footer"><!--Footer-->

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright © 2018 HOLOMOC. All rights reserved.</p>
                <p class="pull-right">Need Help?<span><a target="_blank" href="contact-us.html">Contact Us</a></span></p>
            </div>
        </div>
    </div>
    
</footer><!--/Footer-->